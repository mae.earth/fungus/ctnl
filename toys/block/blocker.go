/* CtNL/toys/blocker.go */
package blocker

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

/* TODO:
 * [feature] add a root-level structure to hold additional information, such as filename (source)
 * [bug] fix the tail statement in the parser
 */

const (
	BufferSize int = 512
	TabWidth   int = 2
)

var (
	ErrNotImplemented = errors.New("Not Implemented")
	ErrMismatched     = errors.New("Mismatched Token Value pairs")
)

/* Block */
type Block struct {
	Statement   string
	Whitespaces int
	Line        int

	Parent   *Block
	Children []*Block
}

/* Options - incomplete tool for fetching values */
type Options map[string]interface{}

/* String */
func (op Options) String(key, def string) string {
	if v, exists := op[key]; exists {
		if b, ok := v.(string); ok {
			return b
		}
	}
	return def
}

/* Int */
func (op Options) Int(key string, def int) int {
	if v, exists := op[key]; exists {
		if b, ok := v.(int); ok {
			return b
		}
	}
	return def
}

/* Float */
func (op Options) Float(key string, def float64) float64 {
	if v, exists := op[key]; exists {
		if b, ok := v.(float64); ok {
			return b
		}
	}
	return def
}

/* Bool */
func (op Options) Bool(key string, def bool) bool {
	if v, exists := op[key]; exists {
		if b, ok := v.(bool); ok {
			return b
		}
	}
	return def
}

/* line */
func line(block *Block) int {
	/* find the last line */
	if len(block.Children) == 0 {
		return block.Line
	}

	return line(block.Children[len(block.Children)-1])
}

/* blockHTML */
func blockHTML(buf *bufio.Writer, block *Block, root bool) {
	d := func(in string) {
		buf.WriteString(in)
	}

	if root {

		d(fmt.Sprintf("<details open>\n<summary>%s<span class=\"line-number\" title=\"line number\">%03d &mdash; %03d</span></summary>\n", block.Statement, block.Line, line(block)))

	} else {

		d(fmt.Sprintf("<li>%s<span class=\"line-number\" title=\"line number\">%03d</span></li>\n", block.Statement, block.Line))

	}

	if len(block.Children) == 0 {
		return
	}

	d("<ul>\n")

	for _, block := range block.Children {

		blockHTML(buf, block, false)
	}

	d("</ul>\n")

	if root {
		d("</details>\n")
	}
}

/* HTML */
func HTML(w io.Writer, blocks []*Block) error {

	buf := bufio.NewWriter(w)
	d := func(in string) {
		buf.WriteString(in)
	}

	d("<!DOCTYPE html>\n")
	d("<html>\n<head>\n")
	d("<style>\n")
	d("body { font-size: 16px; }\n")
	d("ul { list-style: none; padding: 0; margin: 0; margin-left: 20px; }\n")
	d("main > ul { margin-left: 0; }\n")
	d("li,details > summary { padding: 10px; background-color: gainsboro; color: black; margin: 1px 0; }\n")
	d("details { background-color: thistle; margin-top: 15px; }\n")
	d("details > summary { cursor: pointer; }\n")
	d("span.line-number { float: right; font-size: 0.8rem; font-style: italic; }\n")
	d("</style>\n")
	d("</head>\n<body>\n")

	d("<main>\n<ul>\n")

	for _, block := range blocks {

		blockHTML(buf, block, true)
	}

	d("<ul>\n</main>\n</body>\n</html>\n")

	return buf.Flush()
}

/* PrettyPrint */
func PrettyPrint(blocks []*Block, depth int) {

	if depth == 0 {
		fmt.Fprintf(os.Stdout, "\n===\n")
	}

	tabs := strings.Repeat(" ", depth)

	for _, b := range blocks {

		fmt.Fprintf(os.Stdout, "%05d[%02d] %s%s\n", b.Line, b.Whitespaces, tabs, b.Statement)
		PrettyPrint(b.Children, depth+1)

		if depth == 0 {
			fmt.Fprintf(os.Stdout, "\n")
		}

	}

	if depth == 0 {
		fmt.Fprintf(os.Stdout, "\n===\n")
	}

}

/* options */
func options(parameterlist ...interface{}) (map[string]interface{}, error) {

	if len(parameterlist)%2 != 0 {
		return nil, ErrMismatched
	}

	m := make(map[string]interface{}, 0)

	token := ""
	for i, kv := range parameterlist {
		if i%2 == 0 {
			if str, ok := kv.(string); ok {
				token = str
			} else {
				return nil, fmt.Errorf("bad token at %d, was expecting string", i+1)
			}
		} else {

			m[token] = kv
		}
	}

	return m, nil
}

/* ParseToBlocks */
func ParseToBlocks(r io.Reader, parameterlist ...interface{}) ([]*Block, error) {

	/* parser must check each character and pay attention to strings;
	 * the main aim here to so look for newlines and whitespace characters.
	 */

	options, err := options(parameterlist...)
	if err != nil {
		return nil, err
	}

	optVerbose := Options(options).Bool("verbose", false)

	blocks := make([]*Block, 0)
	buf := make([]byte, BufferSize)

	var current *Block

	literal := false
	whitespaces := 0
	content := false
	statement := ""
	line := 0

	for {
		n, err := r.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}

			return nil, err /* TODO: should return more useful information */
		}

		for i := 0; i < n; i++ {
			switch buf[i] {
			case '\n':
				if literal {
					statement += string(buf[i])

				} else {

					line++

					/* this is where the block statement is
					 * written out and then reset for the
					 * next line
					 */
					if optVerbose {
						if len(statement) == 0 {
							fmt.Fprintf(os.Stdout, "%05d\t===========new block============\n", i+1)
						} else {
							fmt.Fprintf(os.Stdout, "%05d\t%03d wsc | %s\n", line, whitespaces, statement)
						}
					}

					/* depth is related to steps in the current
					 * block.
					 */
					if len(statement) == 0 || whitespaces == 0 {
						/* then start new root block */
						current = nil

						if len(statement) > 0 { /* then start new root block */
							current = &Block{Line: line, Statement: statement, Children: make([]*Block, 0), Parent: nil}
							blocks = append(blocks, current)
						}

					} else {

						/* count the depth for the current block and determine
						 * if the new block should be a child or a peer.
						 */
						if current == nil {
							return nil, fmt.Errorf("unexpected whitespace for root statement at line %d", i+1)
						}

						count := 0
						ncurrent := current
						var root *Block
						for {
							if ncurrent.Parent == nil {
								root = ncurrent
								break
							}
							count += ncurrent.Whitespaces
							ncurrent = ncurrent.Parent
						}

						if whitespaces > count {

							if optVerbose {
								fmt.Fprintf(os.Stdout, "\tadded to children (%d > %d)\n", whitespaces, count)
							}

							current.Children = append(current.Children, &Block{Whitespaces: whitespaces, Line: line, Statement: statement, Children: make([]*Block, 0), Parent: current})
							current = current.Children[len(current.Children)-1]

						} else if whitespaces == count {

							if optVerbose {
								fmt.Fprintf(os.Stdout, "\tadded as peer (%d == %d)\n", whitespaces, count)
							}

							if current.Parent == nil {
								return nil, fmt.Errorf("unexpected peer for statement at line %d", line)
							}

							current = current.Parent
							current.Children = append(current.Children, &Block{Whitespaces: whitespaces, Line: line, Statement: statement, Children: make([]*Block, 0), Parent: current})
							current = current.Children[len(current.Children)-1]

						} else {

							if optVerbose {
								fmt.Fprintf(os.Stdout, "\tadded as ancestor (%d < %d)\n", whitespaces, count)
							}

							/* wind up from the root */
							ncurrent = root
							for {

								if len(ncurrent.Children) == 0 {
									break
								}

								if ncurrent.Children[len(ncurrent.Children)-1].Whitespaces == whitespaces {
									break
								}

								ncurrent = ncurrent.Children[len(ncurrent.Children)-1]
							}

							current = ncurrent
							current.Children = append(current.Children, &Block{Whitespaces: whitespaces, Line: line, Statement: statement, Children: make([]*Block, 0), Parent: current})
							current = current.Children[len(current.Children)-1]

						}

					}

					/* reset */
					statement = ""
					whitespaces = 0
					content = false
				}
			case '"', '\'', '`':
				/* TODO: check the pairing */
				literal = !literal
				content = true
				statement += string(buf[i])

			case ' ':

				if !literal && !content {
					whitespaces++
				} else {
					statement += string(buf[i])
				}

			case '\t':

				if !literal && !content {
					whitespaces += TabWidth
				} else {
					statement += string(buf[i])
				}

			default:
				content = true
				statement += string(buf[i])
			}
		}
	}

	return blocks, nil
}
