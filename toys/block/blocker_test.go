/* CtNL/toys/blocker_test.go */
package blocker

import (
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"strings"
	"testing"
)

func Test_Blocker(t *testing.T) {

	Convey("options", t, func() {

		Convey("mismatched pairs", func() {
			m, err := options("a", true, "b")
			So(m, ShouldBeNil)
			So(err, ShouldEqual, ErrMismatched)
		})

		Convey("balanced pairs", func() {
			m, err := options("a", true, "b", "hello", "c", 0.1)
			So(err, ShouldBeNil)
			So(m, ShouldNotBeNil)
			So(len(m), ShouldEqual, 3)
		})

		Convey("read back", func() {
			m, err := options("a", true, "b", "hello", "c", 0.1, "d", 101)
			So(err, ShouldBeNil)
			So(m, ShouldNotBeNil)
			So(len(m), ShouldEqual, 4)

			ops := Options(m)

			Convey("a should be true", func() {
				So(ops.Bool("a", false), ShouldEqual, true)
			})

			Convey("b should be string \"hello\"", func() {
				So(ops.String("b", "bye"), ShouldEqual, "hello")
			})

			Convey("c should be 0.1", func() {
				So(ops.Float("c", -1.0), ShouldEqual, 0.1)
			})

			Convey("d should be 101", func() {
				So(ops.Int("d", 0), ShouldEqual, 101)
			})
		})

	})

	Convey("Blocker", t, func() {

		Convey("simple", func() {

			blocks, err := ParseToBlocks(strings.NewReader(exampleSimple), "filename", "example", "verbose", true)
			So(err, ShouldBeNil)
			So(blocks, ShouldNotBeEmpty)
			So(len(blocks), ShouldEqual, 2)

			So(blocks[0].Statement, ShouldEqual, "root-a")
			So(len(blocks[0].Children), ShouldEqual, 2)

			So(blocks[1].Statement, ShouldEqual, "root-b")
			So(len(blocks[1].Children), ShouldEqual, 2)

			PrettyPrint(blocks, 0)

			f, err := os.Create("simple.html")
			if err != nil {
				panic(err)
			}
			defer f.Close()

			So(HTML(f, blocks), ShouldBeNil)
		})

		Convey("complex", func() {

			blocks, err := ParseToBlocks(strings.NewReader(exampleComplex), "filename", "example", "verbose", true)
			So(err, ShouldBeNil)
			So(blocks, ShouldNotBeEmpty)

			PrettyPrint(blocks, 0)

			f, err := os.Create("complex.html")
			if err != nil {
				panic(err)
			}
			defer f.Close()

			So(HTML(f, blocks), ShouldBeNil)
		})

	})
}

const exampleSimple = `
root-a
  dependant-a1
  dependant-a2

root-b
  dependant-b1
    dependant-b1-1
    dependant-b1-2
	dependant-b2
		dependant-b2-1
    this "here is a string\n"
`

const exampleComplex = `
task "render example.rib"
 with 
  describe function as ground
    channel inflow as tcp and udp
      host as errors.example.org
      use port 999 
  describe function as sink
    channel inflow as udp
      host as sink.exmaple.org
      use port 101
 with library openguts as og
  announce services using mDNS
  default agent backoff 
    map all outflows to ground 
  describe function as gRIB
    do not announce services
    channel outflow as tcp
    set "string file" "example.rib"
    set "string level" "verbose"
    default function og.RIB
  describe program as camera 
    channel inflow as tcp
    channel outflow as tcp 
    default function og.camera
      standard actor
  describe program as world
    channel inflow as tcp
    channel outflow as tcp
    default function og.world
      standard actor
  describe program as raycaster
    channel inflow as tcp
    channel outflow as udp
    default function og.raycast
      standard actor
    set "string outpath" "out"
  map :
    gRIB to camera
    camera to world and raycaster
    world to raycaster 
    raycaster to sink 
`
